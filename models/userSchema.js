var mongoose    =   require("mongoose");
// var mongoSchema =   mongoose.Schema;
// var mongoose    =   require("mongoose");
// create instance of Schema
// create schema
var userSchema  = {
    "userEmail" : {type:String,unique:true,required:true},
    "userPassword" : {type:String,required:true},
    "userFirstName" : {type:String},
    "userLastName" : {type:String},
    "userAddress" : {type:String},
    "userActive":{type:Boolean,default:false},
    "otpValid":{type:Boolean,default:false}
};
// create model if not exists.
module.exports = mongoose.model('userlogin',userSchema);