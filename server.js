var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var router = express.Router();
var mongoose = require("mongoose");
var mongoSchema = mongoose.Schema;
//var config=require("./configration")
var users = require("./models/userSchema");
var key = "secretkey"; //this is secret key
var cors = require('cors')
const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");
var jwt_decode = require('jwt-decode');
var waterfall = require('async-waterfall');
// waterfall(tasks, callback);
const saltRounds = 10;  //it is used to encrypt the password  like key
mongoose.connect('mongodb://localhost:27017/mydb');
//mongoose.connect(config.database);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ "extended": false }));
app.use(cors());

const puretext = require('puretext');
const fromnumber = '+13168030646'
const apitoken = 'nud573'

var nodemailer = require("nodemailer");
var smtpTransport = nodemailer.createTransport({        //this  details are used to send email to register user
    service: "Gmail",
    auth: {
        user: "r131092@rguktrkv.ac.in",
        pass: "3@r!b@bu"
    }
});
var token, mailOptions, host, link, text;

router.post("/otpverify", function (req, res) {
    console.log("coming to otp verification..1111", req.body.otp, "====", text.smsBody)
    if (text.smsBody == req.body.otp) {
        jwt.verify(text.apiToken, key, function (err, decoded) {
            if (err) throw err;
            else {
                var myquery = { userEmail: decoded.emailInToken };
                var newvalues = { $set: { otpValid:true} };
                users.updateOne(myquery, newvalues, function (err, obj) {
                    if (err) throw err;
                    res.status(200).json({ "valid": true });
                })   
            }
        })
    }
    else {
        res.status(200).json({ "valid": false });
    }
})






router.post("/signin", function (req, res) {
    var response = {};
    users.findOne({ userEmail: req.body.email }, function (err, user) {
        // This will run Mongo Query to fetch data based on ID.
        if (!user || user.userActive === false||user.otpValid===false) {
            console.log("coming...to user not registered")
            response = { "data": req.body.email, "message": req.body.email + " is not registered" };
            res.status(200).json(response);
        }
        else {

            bcrypt.compare(req.body.password, user.userPassword, (err, result) => {         //comparing password

                if (err) {
                    console.log("coming to password compare but error")
                    response = { "data": req.body.email, "message": "some error occur" };
                    res.status(200).json(response);
                }
                else if (result) {
                    console.log("coming to generate the token", result);
                    const token = jwt.sign(                     //generationg token
                        {
                            emailInToken: user.userEmail,
                            userIdInToken: user._id
                        },
                        key,                            //process.env.JWT_KEY,
                        {
                            expiresIn: "1h"
                        }
                    )
                    response = { "data": req.body.email, "message": "Login successful\n", "token": token }
                    res.status(200).json(response);


                }
                else {
                    console.log("coming to invalid token")
                    response = { "data": req.body.email, "message": "Invalid Credintials" };
                    res.status(401).json(response);
                }
            });
        }


    })
});



//registration process
router.post("/signup", async function (req, res) {
    var db = new users();
    var response = {};
    console.log("coming..signup method")
    token = jwt.sign(                     //generationg token
        {
            emailInToken: req.body.email
        },
        key,                            //process.env.JWT_KEY,
        {
            expiresIn: "1h"
        }
    )
    console.log("generated token in signup method")
    //rand = Math.floor((Math.random() * 100) + 54);
    host = req.get('host');
    link = "http://" + req.get('host') + "/verify?id=" + token;
    mailOptions = {                                         //here we are creating user details to send email
        to: req.body.email,
        subject: "Please confirm your Email account",
        html: `Hello,<br> Please Click on the link to verify your email.<br><a href=${link}>${link}</a>`
    }
    const rand = Math.floor((Math.random() * 90000) + 54)
    text = {
        // To Number is the number you will be sending the text to.
        toNumber: "+91" + req.body.mobile,
        // From number is the number you will buy from your admin dashboard
        fromNumber: fromnumber,
        // Text Content
        smsBody: rand,
        //Sign up for an account to get an API Token
        apiToken: token

    };
    console.log("coming to signup method and this is text.api token.....", text.smsBody);
    puretext.send(text, function (err, response) {
        if (err) console.log(err);
        else console.log(response)
    })
    console.log(mailOptions);


    // fetch email and password from REST request.
    // Add strict validation when you use this in Production.
    //we use this commmand also======>users.insertMany({userEmail:req.body.email,userPassword:req.body.password})
    users.findOne({ userEmail: req.body.email }, function (err, data) {
        // This will run Mongo Query to fetch data based on ID.
        if (data) {
            console.log("coming to user already exist\n")
            response = { "data": req.body.email, "message": data.userEmail + " is already exist" };
            res.status(200).json(response);
        }
        else {
            smtpTransport.sendMail(mailOptions, function (error, response) {        //here we are sending email
                if (error) {
                    console.log(error);
                    res.end("error");
                } else {
                    console.log("Message sent: " + response);
                    res.end("sent");
                }
            });


            console.log("coming... user not exist\n")
            db.userEmail = req.body.email;
            bcrypt.hash(req.body.password, saltRounds, function (err, hash) {           //encrypting the password
                db.userPassword = hash
                db.save(function (err) {
                    // save() will run insert() command of MongoDB.
                    // it will add new data in collection.
                    console.log("coming...save details")
                    if (err) {
                        console.log("coming...not insert the details")
                        response = { "data": req.body.email, "message": "Error adding data" };
                        res.status(401).json(response);
                    } else {
                        console.log("coming...insert the details")
                        response = { "message": req.body.email + " is register please verify\n" };
                        res.status(200).json(response);
                    }
                });
            })
        }
    });
});

router.put('/update', function (req, res)                //updating the details based on some value
{
    //var decoded = jwt_decode(req.body.token);
    jwt.verify(req.body.token, key, function (err, decoded) {
        if (err) throw err;
        else {
            var myquery = { userEmail: decoded.emailInToken };
            var newvalues = { $set: { userFirstName: req.body.firstname, userLastName: req.body.lastname, userAddress: req.body.address } };
            users.updateOne(myquery, newvalues, function (err, obj) {
                if (err) throw err;
                console.log("1 document updated");
                response = { "data": req.body.email, "message": "Updated", "data": newvalues, "userEmail": myquery };
                res.status(200).json(response);
            }
            );
        }
    })
});

router.delete('/delete', function (req, res)                //updating the details based on some value
{
    //var decoded = jwt_decode(req.body.token);
    jwt.verify(req.body.token, key, function (err, decoded) {
        if (err) throw err;
        else {
            var myquery = { userEmail: decoded.emailInToken };
            users.deleteOne(myquery, function (err, obj) {
                if (err) throw err;
                console.log("1 document deleted");
                response = { "message": decoded.emailInToken + "   deleted" };
                res.status(200).json(response);
            }
            );
        }
    })
});



app.get('/verify', function (req, res) {
    console.log(req.protocol + "://" + req.get('host'));
    if ((req.protocol + "://" + req.get('host')) == ("http://" + host)) {
        console.log("Domain is matched. Information is from Authentic email");
        if (req.query.id == token) {
            jwt.verify(req.query.id, key, function (err, decoded) {
                if (err) throw err;
                else {
                    var myquery = { userEmail: decoded.emailInToken };
                    var newvalues = { $set: { userActive: true } };
                    users.updateOne(myquery, newvalues, function (err, obj) {
                        if (err)
                            throw err;
                        console.log("1 document updated");

                        console.log("email is verified");

                        res.status(200).end(
                            `<a href="http://192.168.43.207:3000/otpverification" >Please verify</a>`

                        );
                    }
                    );
                    ;
                }
            })
        }
        else {
            console.log("email is not verified");
            res.end("<h1>Bad Request</h1>");
        }
    }
    else {
        res.end("<h1>Request is from unknown source");
    }
});


app.use(router);


app.listen(5000);
console.log("Listening to PORT 5000");